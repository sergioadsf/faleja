import { Component, OnInit } from '@angular/core';

import {NgZone} from '@angular/core';

@Component({
	selector: 'chat',
	templateUrl: './app/chat/chat.html',
	styleUrls: ['./app/chat/chat.css']
})
export class ChatDetailComponent {

	static get parameters() {
		return [NgZone];
	}

	socket: any;  
	zone: NgZone;  
	texto: string;
	mensagens: Array<string> = new Array();
	constructor() { 
		this.socket = io('http://localhost:3000');
		this.socket.on('message', (msg) => {
          //  this.zone.run(() => {
                this.mensagens.push(msg);
            //});
        });
	}

	enviar(){
		this.socket.emit('message', {texto: this.texto, idade: 20, nome: 'sergio'});
		this.texto = '';
	}

}