import { NgModule }      from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ChatDetailComponent }   from './chat-detail.component';

@NgModule({
  imports:      [ CommonModule, FormsModule ],
  declarations: [ ChatDetailComponent ],
  exports:    	[ ChatDetailComponent ]
})
export class ChatModule { }