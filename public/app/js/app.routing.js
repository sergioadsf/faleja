"use strict";
const router_1 = require('@angular/router');
const chat_detail_component_1 = require('./chat/chat-detail.component');
const appRoutes = [
    { path: "", component: chat_detail_component_1.ChatDetailComponent }
];
exports.appRoutingProviders = [];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map