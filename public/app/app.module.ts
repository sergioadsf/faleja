import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent }   from './app.component';
import { ChatModule }   from './chat/chat.module';
import { routing, appRoutingProviders } from './app.routing';

@NgModule({
  imports:      [ BrowserModule, routing, ChatModule],
  declarations: [ AppComponent ],
  bootstrap:    [ AppComponent ],
  providers:	[appRoutingProviders]
})
export class AppModule { }