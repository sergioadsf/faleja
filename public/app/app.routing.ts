import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChatDetailComponent }  from './chat/chat-detail.component';

const appRoutes: Routes = [
	{ path: "", component: ChatDetailComponent  }
];

export const appRoutingProviders: any[] = [

];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);