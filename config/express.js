var express = require('express');
var app = express();
var load = require('express-load');
var bodyParser = require('body-parser');
var fs = require('fs');

app.set('secret', 'testetoken');
app.use(express.static('./public'));
app.use("/node_modules", express.static('node_modules'));
app.use(bodyParser.json());

app.get('*', function(req, res, next) {
  next();
});

load('models')
.then('controllers')
.then('routes')
.into(app);

module.exports = app;
