'use strict';

module.exports = function(http){
	var io = require('socket.io')(http);

	io.on('connection', function(socket) {
		console.log('User Connected');
		
		socket.on('message', function(msg){
			let pessoa = msg;
			console.log("m -"+pessoa.nome);
			socket.emit("message", pessoa.texto);
		});
	});
}